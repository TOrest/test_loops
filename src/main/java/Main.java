import tools.*;
import java.lang.Math;

public class Main {

    public static void main(String[] args) {
        final int max = 1100;
        final int x = (int)(Math.random() * (max + 1));
        final int y = (int)(Math.random() * (max + 1));
        int [][] array = new int[100][100];
        Tools action = new Tools();
        action.fillArray(array, 10, 100);
        action.print2DArray(array);
    }
}
