package tools;

import org.w3c.dom.ls.LSOutput;

public class Tools {

    public void print2DArray(int [][] array)
    {
        if(array.length == 0)
        {
            System.out.println("There is no array here");
        }
        else
        {
            if(array.length >= 2 && array[0].length >= 3)
            {

                for(int i = 0; i < array.length; i++)
                {
                    for(int j = 0; j < array[i].length; j++)
                    {
                        if(i == 1 && j == 2)
                        {
                            System.out.print("   ");
                        }
                        else
                        {
                            System.out.print(array[i][j] + " ");
                        }
                    }
                    System.out.println(" ");
                    if(i == 1000)
                    {
                        System.out.println("There are too many rows. Printed only 1000");
                        break;
                    }
                }
            }
            else
            {
                for(int i = 0; i < array.length; i++)
                {
                    for(int j = 0; j < array[i].length; j++)
                    {
                        System.out.print(array[i][j] + " ");
                    }
                    System.out.println();
                }
            }
        }
    }

    public int[][] fillArray(int [][] array, int min_value, int max_value)
    {
        for(int i = 0; i < array.length; i++)
        {
            for(int j = 0; j < array[0].length; j++)
            {
                array[i][j] = (int)(Math.random() * ((max_value - min_value) + 1) + min_value);
            }
        }
        return array;
    }
}
